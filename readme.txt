=== Plugin Name ===

Contributors: Luigi Briganti
Plugin Name: Linebreak Opengraph
Plugin URI: http://www.linebreak.it/
Tags: wp, facebook, open graph, opengraph
Author URI: http://www.linebreak.it/
Author: Linebreak
Donate link: https://paypal.me/luizbriganti
Requires at least: 4.3
Tested up to: 5.1
Stable tag: 0.1
Version: 0.1

== Description ==

This plugin allow you to append opengraph meta data to your website head, without need to perform the scraping of your site with Facebook debugger

== Installation ==

If not installing it directly from the Wordpress Plugin Repository, in your website wp-admin go to Plugins->Add new
and upload the zip folder of the plugin and wait for the installation to be completed. After that, if no errors occurred,
activate plugin and then you are ready to go.

== Upgrade Notice ==

== Screenshoots ==

1. In customizer, you can find the section for adding a custom Open Graph image under "Site title".
2. You can upload your image as for any other image in wordpress.
3. These are the meta og tags that are generated for each article. If no custom image has been set, the post or page thumbnail will be loaded instead.

/linebreak-opengraph/assets/screenshot-1.jpg
/linebreak-opengraph/assets/screenshot-2.jpg
/linebreak-opengraph/assets/screenshot-3.jpg

svn propset svn:mime-type image/jpeg *.jpg

== Donations ==

If you want to support us, you can donate to paypal.me/luizbriganti. Thanks, in advance!
