<?php
/**
 * @package Linebreak Opengraph
 * @version 0.1
 */
/*
Plugin Name: Linebreak Opengraph
Plugin URI: /
Description: This plugin allow you to append opengraph meta data to your website head, without need to perform the scraping of your site with Facebook debugger
Author: Luigi Briganti
Version: 0.1
Author URI: http://www.linebreak.it
Text domain: linebreak-og
License: GNU General Public License v2

*/

/**
 * Text domain and security tags
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit; // don't access directly
 };

 define( 'lbog_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

/**
 * Declaring main functions
 */

if(!function_exists('linebreak_open_graph')):
  function linebreak_open_graph(){

    if(is_front_page() || is_home()){ ?>
      <meta property="og:type" content="website">
      <meta property="og:url" content="<?php echo get_site_url(); ?>">
      <meta property="og:title" content="<?php echo get_bloginfo('name'); ?>">
      <meta property="og:image" content="<?php if(has_post_thumbnail()): echo get_the_post_thumbnail_url(get_the_ID(), 'full'); else: echo get_theme_mod('lb_open_graph_image'); endif; ?>">
      <meta property="og:description" content="<?php echo get_bloginfo('description'); ?>">
      <meta property="og:locale" content="it_IT">
<?php } elseif(is_single()){
          global $post;
          $excerpt = substr(strip_tags($post->post_content), 0, 160);
  ?>
      <meta property="og:type" content="article">
      <meta property="og:url" content="<?php echo get_permalink(); ?>">
      <meta property="og:title" content="<?php echo get_the_title(); ?>">
      <meta property="og:image" content="<?php if(has_post_thumbnail()): echo get_the_post_thumbnail_url(get_the_ID(), 'full'); else : echo get_theme_mod('lb_open_graph_image'); endif; ?>">
      <meta property="og:description" content="<?php echo str_replace('', '&nbsp;', $excerpt); ?>">
      <meta property="og:locale" content="it_IT">
      <meta property="og:site_name" content="<?php echo get_bloginfo('name'); ?>">
<?php }
  }

  add_action('wp_head', 'linebreak_open_graph', 0);

endif;

if(!function_exists('linebreak_customize_register')):
  function linebreak_customize_register($wp_customize){
    $wp_customize->add_setting('lb_open_graph_image', array(
        'transport'     => 'refresh',
				'type'					=> 'theme_mod',
    ));

  	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lb_open_graph_image', array(
  	      'label' => __('Upload an image for Open Graph', 'linebreak-og'),
  	      'section' => 'title_tagline',
  	      'settings' => 'lb_open_graph_image',
  	)));
  }
  add_action( 'customize_register', 'linebreak_customize_register' );
endif;
